import './App.css';
import Fixtures from './Fixtures';

function App() {
  return (
    <div className="App">
      <h4>Fixtures</h4>
      <hr/>
        <Fixtures/>
    </div>
  );
}

export default App;
