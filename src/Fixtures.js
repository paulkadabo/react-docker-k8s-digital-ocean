import React, {useState, useEffect} from 'react'

const Fixtures = () => {
    const [fixtures, setFixtures] = useState([])

    const BACKEND_API_URL = "http://64.225.82.91:8000/"

    useEffect(() => {
        getFixtures();
    }, [])

    let getFixtures = async () => {
        let response = await fetch(BACKEND_API_URL, {
            method: "GET"
        })
        let data = await response.json()
        setFixtures(data)
    }
    console.log("Fixtures: ", fixtures)
  return (
    <div>
      {fixtures.map((fixture) => {
        return (
          <p key={fixture.id}>
            {fixture.home_team} Vs. {fixture.away_team}
          </p>
        );
      })}
    </div>
  );
}

export default Fixtures